# Technical Hugo

Technical Hugo is a project created as a learning experience of the Hugo static site generator.

## Installing

To build the site, you'll need to install the [Hugo static site generator](https://gohugo.io/).

## Building

To build the site, you can use the `hugo` command. The site will be published in `/public` and be ready to be deployed.