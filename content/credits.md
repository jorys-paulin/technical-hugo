---
title: "Credits"
date: 2021-03-14T15:30:00+01:00
---

Technical Hugo was created using the [Hugo static site generator](https://gohugo.io/), following the examples provided in the [documentation](https://gohugo.io/documentation/)